package main

import (
	"errors"
	"fmt"
	"log"
	"net/http"
	"net/url"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/rijksdienst-realistische-demos-frontend/backend"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/rijksdienst-realistische-demos-frontend/config"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/rijksdienst-realistische-demos-frontend/model"
)

func main() {
	// Template engine and functions
	engine := html.New("./views", ".html")

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",

		// Override default error handler
		ErrorHandler: func(c *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError

			// Retrieve the custom status code if it's a *fiber.Error
			var e *fiber.Error
			if errors.As(err, &e) {
				code = e.Code
			}

			// Send custom error page
			if err = c.Render("error", fiber.Map{
				"title":   fmt.Sprintf("Fout %d", code),
				"code":    code,
				"details": err.Error(),
			}); err != nil {
				// In case serving the error page the fails
				return c.Status(code).SendString(err.Error())
			}

			// Return from handler
			return nil
		},
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Healthz endpoint
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendString(".") // Send a period as response body, similar to chi's Heartbeat middleware
	})

	// Business register
	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the businesses from the Fictief Handelsregister backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10")) // IMPROVE: use implicitly, do not include in query parameters?
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		var response model.Response[model.Business]
		if err := backend.Request(http.MethodGet, config.Config.FHR.Domain, fmt.Sprintf("/businesses?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.FHR.FSCGrantHash}, &response); err != nil {
			return fmt.Errorf("error fetching businesses: %v", err)
		}

		return c.Render("index", fiber.Map{
			"title":      "Handelsregister",
			"businesses": response.Data,
			"pagination": response.Pagination,
		})
	})

	// People
	people := app.Group("/people")

	people.Get("/", func(c *fiber.Ctx) error {
		return c.Render("person-index", fiber.Map{
			"title": "Landelijk Register Personen",
		})
	})

	// Start server
	if err := app.Listen(config.Config.ListenAddress); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
