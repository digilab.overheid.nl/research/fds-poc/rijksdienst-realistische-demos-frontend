package model

import "github.com/google/uuid"

type Business struct {
	ID            uuid.UUID `json:"id"`
	RSIN          string    `json:"rsin"`
	StatutoryName string    `json:"statutoryName"`
}
